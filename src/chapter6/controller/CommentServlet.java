package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String comment = request.getParameter("text");
		if (!isValid(comment, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		Comment comments = new Comment();
		comments.setText(comment);

		User user = (User) session.getAttribute("loginUser");
		comments.setUserId(user.getId());
		comments.setMessageId(Integer.parseInt(request.getParameter("id")));

		new CommentService().insert(comments);
		response.sendRedirect("./");

	}

	private boolean isValid(String comment, List<String> errorMessages) {

		if (StringUtils.isBlank(comment)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < comment.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}

