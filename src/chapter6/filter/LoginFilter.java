package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebFilter(urlPatterns={"/setting", "/edit"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		//セッション内にloginuserが無い場合(ログインしていない場合)

		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		HttpSession session = req.getSession();

		if(session.getAttribute("loginUser") != null) {
			chain.doFilter(request, response);
		}else {
			session.setAttribute("errorMessages", "ログインしてください");
			res.sendRedirect("login.jsp");
			return;
		}
	}



	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}

